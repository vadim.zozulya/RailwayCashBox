﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using RailwayCashBox.DAL.Contexts;
using RailwayCashBox.DAL.Entities;

namespace RailwayCashBox.DAL.Repositories
{
    public class Repository : IDisposable
    {
        Context _db;

        public Repository()
        {
            _db = new Context();
        }

        // Все маршруты
        public List<TrainRoute> GetAllTrainRoutes()
        {
            return _db.TrainRoutes.ToList<TrainRoute>(); 
        }

        public IList<TrainRoute> GetRoutes()
        {
            using (var t = new Context())
            {
                var routes = t.TrainRoutes
                    .Include(x => x.StartStation)
                    .Include(x=>x.StartStation.Station)
                    .Include(x=>x.EndStation.Station)
                    .AsNoTracking()
                    .ToList();
                return routes;
            }
        }

        // маршрут по Id
        public TrainRoute GetTrainRoute(int id)
        {
            var route = _db.TrainRoutes
                .Include(x => x.StartStation.Station)
                .Include(x => x.IntermediateStations)
				.Include(x => x.IntermediateStations.Select(s => s.Station))
				.Include(x => x.EndStation.Station)
				.Include(x => x.TrainCars)
				.FirstOrDefault(x=>x.Id==id);
            
            return route;
        }

        // создать маршрут
        public void CreateTrainRoute(TrainRoute trainRoute)
        {
            _db.TrainRoutes.Add(trainRoute);
            _db.SaveChanges();
        }

        // удалить маршрут
        public void DeleteTrainRoute(int id)
        {
	        var route = GetTrainRoute(id);

	        if (route.StartStation != null)
	        {
		        _db.StationStops.Remove(route.StartStation);
	        }
	        if (route.EndStation != null)
	        {
		        _db.StationStops.Remove(route.EndStation);
	        }
	        foreach (var intermediateStation in route.IntermediateStations.ToList())
	        {
				_db.StationStops.Remove(intermediateStation);
			}
	        foreach (var trainCar in route.TrainCars.ToList())
	        {
		        _db.TrainCars.Remove(trainCar);
	        }

	        var records = _db.TrainScheduleRecords.Where(r => r.TrainRoute.Id == id)
				.Include(r => r.SeatBookings)
				.ToList();

	        foreach (var trainScheduleRecord in records)
	        {
		        var orders = _db.Orders.Where(o => o.TrainScheduleRecord.Id == trainScheduleRecord.Id).ToList();
		        foreach (var order in orders)
		        {
			        order.TrainScheduleRecord = null;
					_db.Entry(order).State = EntityState.Modified;
		        }

				_db.SeatBookings.RemoveRange(trainScheduleRecord.SeatBookings);
				_db.TrainScheduleRecords.Remove(trainScheduleRecord);
			}

			_db.TrainRoutes.Remove(route);
            _db.SaveChanges();
        }

        // изменить маршрут
        public void EditTrainRoute(TrainRoute trainRoute)
        {
            _db.Entry(trainRoute).State = EntityState.Modified;
            foreach (var t in trainRoute.IntermediateStations)
                _db.Entry(t).State = EntityState.Modified;
            foreach (var t in trainRoute.TrainCars)
                _db.Entry(t).State = EntityState.Modified;
            _db.SaveChanges();
        }

        // Названия всех станций
        public List<String> GetStationStopsTitle
        {
            get { return _db.Stations.Select(x => x.Name).ToList<String>(); }
        }

        // Список всех станций
        public List<Station> GetAllStations()
        {
	        return _db.Stations.ToList();
        }

        // станция по Id
        public Station GetStation(Int32 Id)
        {
            return _db.Stations.Find(Id);
        }

        // создать станцию
        public void CreateStation(Station station)
        {
            _db.Stations.Add(station);
            _db.SaveChanges();
        }

        // удалить станцию
        public void DeleteStation(int id)
        {
            _db.Stations.Remove(_db.Stations.Find(id));
            _db.SaveChanges();
        }

        // изменить станцию
        public void EditStations(Station station)
        {
            _db.Entry(station).State = EntityState.Modified;
            _db.SaveChanges();
        }

	    public List<TrainScheduleRecord> GetSchedule(int routeId)
	    {
		    return
			    _db.TrainScheduleRecords.Where(r => r.TrainRoute.Id == routeId)
                    
				    .Include(r => r.TrainRoute)
				    .Include(r => r.SeatBookings)
					.Include(r => r.TrainRoute.StartStation.Station)
					.Include(r => r.TrainRoute.EndStation.Station)
					.ToList();
	    }

		public void ClearScheduleForRoute(int routeId)
		{
			var toRemove = GetSchedule(routeId);
			foreach (var item in toRemove.ToList())
			{
				_db.SeatBookings.RemoveRange(item.SeatBookings);
				_db.TrainScheduleRecords.Remove(item);
			}
            _db.SaveChanges();
		}

		public void Dispose()
	    {
		    if (_db != null)
		    {
				_db.Dispose();
			    _db = null;
		    }
	    }

	    public TrainScheduleRecord GetTrainScheduleRecord(int id)
	    {
		    return _db.TrainScheduleRecords
				.Include(r => r.TrainRoute)
				.Include(r => r.SeatBookings)
				.Include(r => r.SeatBookings.Select(x => x.TrainCar))
				.Include(r => r.TrainRoute.StartStation.Station)
				.Include(r => r.TrainRoute.EndStation.Station)
				.Include(r => r.TrainRoute.TrainCars)
				.First(r => r.Id == id);
	    }

	    public void DeleteTrainScheduleRecord(int id)
	    {
		    var record = _db.TrainScheduleRecords
				.Include(r => r.SeatBookings)
				.First(r => r.Id == id);
			_db.SeatBookings.RemoveRange(record.SeatBookings);
			_db.TrainScheduleRecords.Remove(record);
            _db.SaveChanges();
		}

	    public void SaveSchedule(List<TrainScheduleRecord> records)
	    {
		    foreach (var trainScheduleRecord in records)
		    {
			    _db.TrainScheduleRecords.Add(trainScheduleRecord);
		    }
            _db.SaveChanges();
		}

	    public List<TrainScheduleRecord> GetScheduleForStationsAndDate(int startStationId, int endStationId, DateTime startDate, DateTime? endDate)
	    {
		    var query = _db.TrainScheduleRecords
			    .Where(r => (r.TrainRoute.StartStation.Station.Id == startStationId ||
			                 r.TrainRoute.IntermediateStations.Any(s => s.Station.Id == startStationId))
			                &&
			                (r.TrainRoute.EndStation.Station.Id == endStationId ||
			                 r.TrainRoute.IntermediateStations.Any(s => s.Station.Id == endStationId)));

			if (endDate.HasValue)
				query = query.Where(r =>
					DbFunctions.TruncateTime(r.DepartureDate) >= startDate
					&&
					DbFunctions.TruncateTime(r.DepartureDate) <= endDate);
			else
			{
				query = query.Where(r => DbFunctions.TruncateTime(r.DepartureDate) == startDate);
			}

			var result = query.Include(r => r.TrainRoute)
				.Include(r => r.TrainRoute.StartStation)
				.Include(r => r.TrainRoute.StartStation.Station)
				.Include(r => r.TrainRoute.EndStation)
				.Include(r => r.TrainRoute.EndStation.Station)
				.Include(r => r.TrainRoute.IntermediateStations)
				.Include(r => r.TrainRoute.IntermediateStations.Select(x => x.Station))
				.Include(r => r.SeatBookings)
				.ToList();

			return result;
	    }

	    public void SaveOrder(Order order)
	    {
		    foreach (var seatBooking in order.SeatBookings)
		    {
			    _db.Entry(seatBooking).State = EntityState.Modified;
		    }

		    _db.TrainScheduleRecords.Attach(order.TrainScheduleRecord);

		    _db.Orders.Add(order);
		    _db.SaveChanges();
	    }

	    public List<Order> GetOrders(string userId)
	    {
		    return _db.Orders
				.Include(o => o.SeatBookings)
				.Include(o => o.TrainScheduleRecord)
				.Include(o => o.TrainScheduleRecord.TrainRoute)
				.Include(o => o.TrainScheduleRecord.SeatBookings)
				.Include(o => o.TrainScheduleRecord.TrainRoute.StartStation)
				.Include(o => o.TrainScheduleRecord.TrainRoute.StartStation.Station)
				.Include(o => o.TrainScheduleRecord.TrainRoute.EndStation)
				.Include(o => o.TrainScheduleRecord.TrainRoute.EndStation.Station)
				.Where(o => o.UserId == userId)
				.ToList();
	    }

	    public void DeleteOrder(int id)
	    {
		    var order = _db.Orders
				.Include(o => o.SeatBookings)
				.Include(o => o.SeatBookings.Select(s=> s.TrainCar))
				.Include(o => o.TrainScheduleRecord)
				.First(o => o.Id == id);

		    var trainScheduleRecord = order.TrainScheduleRecord;
			var seatBookings = order.SeatBookings.ToList();

		    foreach (var seatBooking in order.SeatBookings)
		    {
			    seatBooking.Booked = false;
		    }

			_db.Orders.Remove(order);

			if(trainScheduleRecord != null)
				_db.Entry(trainScheduleRecord).State = EntityState.Unchanged;

			foreach (var seatBooking in seatBookings)
		    {
				_db.Entry(seatBooking).State = EntityState.Modified;
			}

			_db.SaveChanges();
	    }

	    public void DeleteUser(string id)
	    {
			var userStore = new UserStore<ApplicationUser>(_db);
			var userManager = new UserManager<ApplicationUser>(userStore);
			var user = userManager.Users.First(u => u.Id == id);
		    userManager.Delete(user);
		    _db.SaveChanges();
	    }

	    public List<ApplicationUser> GetManagers()
	    {
			var roleStore = new RoleStore<IdentityRole>(_db);
			var roleManager = new RoleManager<IdentityRole>(roleStore);
		    var role = roleManager.Roles.First(r => r.Name == "manager");

			var userStore = new UserStore<ApplicationUser>(_db);
			var userManager = new UserManager<ApplicationUser>(userStore);
			var users = userManager.Users.Where(u => u.Roles.Any(r => r.RoleId == role.Id)).ToList();
		    return users;
	    }

	    public ApplicationUser GetManager(string id)
	    {
			var userStore = new UserStore<ApplicationUser>(_db);
			var userManager = new UserManager<ApplicationUser>(userStore);
			var user = userManager.Users.First(u => u.Id == id);
			return user;
	    }

	    public void SaveManager(string userId, string userName)
	    {
			var userStore = new UserStore<ApplicationUser>(_db);
			var userManager = new UserManager<ApplicationUser>(userStore);
			var user = userManager.Users.First(u => u.Id == userId);
		    user.UserName = userName;
		    _db.SaveChanges();
		}

	    public void ChangePassword(string userId, string password)
	    {
			var userStore = new UserStore<ApplicationUser>(_db);
			var userManager = new UserManager<ApplicationUser>(userStore);
		    userManager.RemovePassword(userId);
		    userManager.AddPassword(userId, password);
		    _db.SaveChanges();
	    }

	    public void AddManager(string userName, string password)
	    {
			var userStore = new UserStore<ApplicationUser>(_db);
			var userManager = new UserManager<ApplicationUser>(userStore);
			var user = new ApplicationUser { UserName = userName };
			userManager.Create(user, password);
			user = userManager.Users.First(u => u.UserName == userName);
			userManager.AddToRole(user.Id, "manager");
		}
    }
}