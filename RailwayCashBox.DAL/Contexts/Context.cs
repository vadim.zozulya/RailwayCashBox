﻿using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;
using RailwayCashBox.DAL.Entities;

namespace RailwayCashBox.DAL.Contexts
{
	public class Context : IdentityDbContext<ApplicationUser>
	{
		public Context()
			: base("MyContext", false)
		{
		}

		public static Context Create()
		{
			return new Context();
		}

		public DbSet<TrainRoute> TrainRoutes { get; set; }
		public DbSet<StationStop> StationStops { get; set; }
		public DbSet<TrainCar> TrainCars { get; set; }
		public DbSet<Station> Stations { get; set; }
		public DbSet<Order> Orders { get; set; }
		public DbSet<TrainScheduleRecord> TrainScheduleRecords { get; set; }
		public DbSet<SeatBooking> SeatBookings { get; set; }

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);
		}
	}
}