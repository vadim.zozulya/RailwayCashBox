﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using RailwayCashBox.DAL.Entities;

namespace RailwayCashBox.DAL.Contexts
{
    public class ContextInitializator : DropCreateDatabaseIfModelChanges<Context>
    {
        protected override void Seed(Context context)
		{
			List<Station> stations = new List<Station>()
			{
				new Station { Name = "Киев"},
				new Station { Name = "Харьков"},
				new Station { Name = "Полтава" },
				new Station { Name = "Миргород" },
				new Station { Name = "Одесса" },
				new Station { Name = "Запорожье" }
			};
			stations.ForEach(s => context.Stations.Add(s));
			context.SaveChanges();

			var route = CreateTestRoute(context);
            context.TrainRoutes.Add(route);
            context.SaveChanges();

			var roleStore = new RoleStore<IdentityRole>(context);
			var roleManager = new RoleManager<IdentityRole>(roleStore);
			var adminRole = new IdentityRole { Name = "admin" };
			roleManager.Create(adminRole);
			context.SaveChanges();

			var userStore = new UserStore<ApplicationUser>(context);
			var userManager = new UserManager<ApplicationUser>(userStore);
			var user = new ApplicationUser { UserName = "admin" };
			userManager.Create(user, "asdfgh");
			user = userManager.Users.First(u => u.UserName == "admin");
			userManager.AddToRole(user.Id, "admin");

			var editorRole = new IdentityRole { Name = "manager" };
			roleManager.Create(editorRole);

			var userRole = new IdentityRole { Name = "user" };
			roleManager.Create(userRole);
		}

		//тестовый маршрут
		public static TrainRoute CreateTestRoute(Context context)
        {
            var kiyv = context.Stations.FirstOrDefault(s => s.Name == "Киев");
            var kharkov = context.Stations.FirstOrDefault(s => s.Name == "Харьков");
            var poltava = context.Stations.FirstOrDefault(s => s.Name == "Полтава");

			var startDate = new DateTime(DateTime.Today.Year, 2, 1);

	        var startTime = startDate.Add(new TimeSpan(2, 30, 0));
            var intermediateStationArriveTime = startDate.Add(new TimeSpan( 4, 0, 0));
            var intermediateStationDepartureTime = intermediateStationArriveTime.Add(TimeSpan.FromMinutes(20));
            var endTime = startDate.Add(new TimeSpan( 17, 45, 0));

            var startStation = new StationStop { Station = kiyv, DepartureTime = startTime };
            var intermediateStation = new StationStop { Station = poltava, ArrivalTime = intermediateStationArriveTime, DepartureTime = intermediateStationDepartureTime };
            var endStation = new StationStop { Station = kharkov, ArrivalTime = endTime };

            var trainCars = new List<TrainCar>
            {
                new TrainCar {Number = 1, CarType = CarType.SoftSleeper, SeatPrice = 300},
                new TrainCar {Number = 2, CarType = CarType.SoftSleeper, SeatPrice = 300},
                new TrainCar {Number = 3, CarType = CarType.SoftSleeper, SeatPrice = 300},
                new TrainCar {Number = 4, CarType = CarType.HardSleeper, SeatPrice = 210},
                new TrainCar {Number = 5, CarType = CarType.HardSleeper, SeatPrice = 210},
                new TrainCar {Number = 6, CarType = CarType.HardSleeper, SeatPrice = 210},
                new TrainCar {Number = 10, CarType = CarType.HardSleeper, SeatPrice = 210},
                new TrainCar {Number = 12, CarType = CarType.HardSleeper, SeatPrice = 210},
                new TrainCar {Number = 13, CarType = CarType.HardSleeper, SeatPrice = 210},
            };

	        var route = new TrainRoute
            {
                Code = "63A",
                StartStation = startStation,
                EndStation = endStation,
                IntermediateStations = new List<StationStop> { intermediateStation },
                TrainCars = trainCars,
                StartDate = startDate,
                ScheduleType = ScheduleType.Each2Day
            };

            return route;
        }
    }
}