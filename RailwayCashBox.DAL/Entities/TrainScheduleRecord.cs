﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace RailwayCashBox.DAL.Entities
{
    public class TrainScheduleRecord
    {
        public int Id { get; set; }

        [DisplayName("Маршрут")]
        public TrainRoute TrainRoute { get; set; }

        [DisplayName("Дата отправления")]
		[DataType(DataType.DateTime)]
        public DateTime DepartureDate { get; set; }

        [DisplayName("Дата прибытия")]
		[DataType(DataType.DateTime)]
		public DateTime ArrivalDate { get; set; }

        [DisplayName("Билеты")]
        public List<SeatBooking> SeatBookings { get; set; }
    }
}