﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using RailwayCashBox.DAL.Helpers;

namespace RailwayCashBox.DAL.Entities
{
    public class TrainCar
    {
		public int Id { get; set; }

	    public TrainRoute TrainRoute { get; set; }

	    [DisplayName("Номер вагона")]
		[Required]
		public int Number { get; set; }

        [DisplayName("Тип вагона")]
		[Required]
		public CarType CarType { get; set; }

        [DisplayName("Стоимость билета")]
		[Required]
		public decimal SeatPrice { get; set; }

        public int GetNumberOfSeats()
        {
            return NumberOfSeatsUtils.GetStandartNumberOfSeats(CarType);
        }
    }
}