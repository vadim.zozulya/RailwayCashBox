﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace RailwayCashBox.DAL.Entities
{
    public class TrainRoute
    {
        public TrainRoute()
        {
            IntermediateStations = new List<StationStop>();
            TrainCars = new List<TrainCar>();
			StartStation = new StationStop();
			EndStation = new StationStop();
		}

		public int Id { get; set; }

        [DisplayName("Номер маршрута")]
		[Required]
        public string Code { get; set; }

        [DisplayName("Начальная станция")]
		public StationStop StartStation { get; set; }

        [DisplayName("Конечная станция")]
		public StationStop EndStation { get; set; }

        [DisplayName("Промежуточные станции")]
		public List<StationStop> IntermediateStations { get; set; }

        [DisplayName("Вагоны поезда")]
        public List<TrainCar> TrainCars { get; set; }

        [DisplayName("Дата начала маршрута")]
		[DataType(DataType.Date)]
		[Required]
        public DateTime StartDate { get; set; }

        [DisplayName("Дата окончания маршрута")]
		[DataType(DataType.Date)]
		public DateTime? StopDate { get; set; }

        [DisplayName("Расписание/график")]
		[Required]
        public ScheduleType ScheduleType { get; set; }
    }
}