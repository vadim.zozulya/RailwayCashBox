﻿using System;
using System.ComponentModel.DataAnnotations;

namespace RailwayCashBox.DAL.Entities
{
    public enum CarType
    {
		[Display(Name = "экспресс")]
		SoftSeat = 1, //only for seating
		[Display(Name = "плацкарт")]
		HardSleeper = 2, //6 sleeping places per coupe
		[Display(Name = "купе")]
		SoftSleeper = 3, //4 sleeping places per coupe
		[Display(Name = "св")]
		SoftSleeperDeluxe = 4 //2 sleeping places per coupe
    }

	public static class CarTypeToDescription
	{
		public static string ToDescription(Enum enumArg)
		{
			var member = enumArg is CarType ? (CarType) enumArg : CarType.SoftSleeper;

			switch (member)
			{
				case CarType.SoftSeat:
					return "экспресс";
				case CarType.HardSleeper:
					return "плацкарт";
				case CarType.SoftSleeper:
					return "купе";
				case CarType.SoftSleeperDeluxe:
					return "св";
			}
			return null;
		}
	}
}