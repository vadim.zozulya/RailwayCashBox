﻿using System;

namespace RailwayCashBox.DAL.Entities
{
    public enum ScheduleType
    {
        Each1Day = 1,
        Each2Day = 2,
        Each3Day = 3,
        Each4Day = 4,
        Each5Day = 5,
        Each6Day = 6,
        Each7Day = 7
    }
	public static class ScheduleTypeToDescription
	{
		public static string ToDescription(Enum enumArg)
		{
			var member = enumArg is ScheduleType ? (ScheduleType)enumArg : ScheduleType.Each1Day;

			switch (member)
			{
				case ScheduleType.Each1Day:
					return "Каждый день";
				case ScheduleType.Each2Day:
					return "Каждые 2 дня";
				case ScheduleType.Each3Day:
					return "Каждые 3 дня";
				case ScheduleType.Each4Day:
					return "Каждые 4 дня";
				case ScheduleType.Each5Day:
					return "Каждые 5 дней";
				case ScheduleType.Each6Day:
					return "Каждые 6 дней";
				case ScheduleType.Each7Day:
					return "Каждые 7 дней";
			}
			return null;
		}
	}
}