﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RailwayCashBox.DAL.Entities
{
    public class StationStop
    {
		public int Id { get; set; }

	    private int? _stationIdForEditing;
		[NotMapped]
	    public int? StationIdForEditing
	    {
			get
			{
				return _stationIdForEditing ?? (_stationIdForEditing = Station == null? (int?)null : Station.Id);
			}
			set
			{
				_stationIdForEditing = value;
				Station = null;
			}
	    }

	    public Station Station { get; set; }

        [DisplayName("Время прибытия")]
		[DataType(DataType.DateTime)]
		public DateTime? ArrivalTime { get; set; }

        [DisplayName("Время отправления")]
		[DataType(DataType.DateTime)]
		public DateTime? DepartureTime { get; set; }
    }
}