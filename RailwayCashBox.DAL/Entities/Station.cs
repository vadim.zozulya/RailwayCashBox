﻿using System.ComponentModel;

namespace RailwayCashBox.DAL.Entities
{
    public class Station
    {
		public int Id { get; set; }

        [DisplayName("Название станции")]
        public string Name { get; set; }
    }
}