﻿using System.ComponentModel;

namespace RailwayCashBox.DAL.Entities
{
    public class SeatBooking
    {
		public int Id { get; set; }

		public TrainCar TrainCar { get; set; }

        [DisplayName("Место")]
        public int SeatNumber { get; set; }

        [DisplayName("Забронировано")]
        public bool Booked { get; set; }
    }
}