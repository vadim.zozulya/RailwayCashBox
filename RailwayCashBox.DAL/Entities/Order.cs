﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RailwayCashBox.DAL.Entities
{
    public class Order
    {
        public Order()
        {
            SeatBookings = new List<SeatBooking>();
        }
        
		public int Id { get; set; }

	    public TrainScheduleRecord TrainScheduleRecord { get; set; }
	    [Display(Name = "Забронированные места")]
        public List<SeatBooking> SeatBookings { get; set; }
		[Display(Name = "Сумма")]
		public decimal TotalPrice { get; set; }
		[Display(Name = "Клиент")]
		public string UserId { get; set; }
    }
}