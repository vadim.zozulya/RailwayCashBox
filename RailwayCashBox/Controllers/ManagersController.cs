﻿using System.Linq;
using System.Web.Mvc;
using RailwayCashBox.DAL.Repositories;
using RailwayCashBox.Models;

namespace RailwayCashBox.Controllers
{
	[Authorize(Roles = "admin")]
	public class ManagersController : Controller
	{
        private Repository db = new Repository();

		public ActionResult Index()
		{
			var managers = db.GetManagers().Select(m => new SecurityAccount() {UserId = m.Id, UserName = m.UserName});
			return View(managers);
		}

		public ActionResult Create()
		{
			return View(new SecurityAccount());
		}

		[HttpPost]
		public ActionResult Create(SecurityAccount securityAccount)
		{
			db.AddManager(securityAccount.UserName, securityAccount.Password);
			return RedirectToAction("Index");
		}

		public ActionResult Edit(string id)
		{
			var manager = db.GetManager(id);
			return View(new SecurityAccount {UserId = manager.Id, UserName = manager.UserName});
		}

		[HttpPost]
		public ActionResult Edit(SecurityAccount securityAccount)
		{
			db.SaveManager(securityAccount.UserId, securityAccount.UserName);
			return RedirectToAction("Index");
		}

		public ActionResult ChangePassword(string id)
		{
			var manager = db.GetManager(id);
			return View(new SecurityAccount { UserId = manager.Id, UserName = manager.UserName });
		}

		[HttpPost]
		public ActionResult ChangePassword(SecurityAccount securityAccount)
		{
			db.ChangePassword(securityAccount.UserId, securityAccount.Password);
			return RedirectToAction("Index");
		}

		public ActionResult Delete(string id)
		{
			var manager = db.GetManager(id);
			return View(new SecurityAccount { UserId = manager.Id, UserName = manager.UserName });
		}

		[HttpPost]
		public ActionResult Delete(SecurityAccount account)
		{
			db.DeleteUser(account.UserId);
			return RedirectToAction("Index");
		}

		protected override void Dispose(bool disposing)
		{
			db.Dispose();
			base.Dispose(disposing);
		}
	}
}