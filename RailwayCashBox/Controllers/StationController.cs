﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RailwayCashBox.DAL.Entities;
using RailwayCashBox.DAL.Repositories;

namespace RailwayCashBox.Controllers
{
	[Authorize(Roles = "manager, admin")]
	public class StationController : Controller
    {
        private Repository db = new Repository();

        public ActionResult Index()
        {
            return View(db.GetAllStations());
        }

		public ActionResult Details(int id = 0)
        {
            Station station = db.GetStation(id);
            if (station == null)
            {
                return HttpNotFound();
            }
            return View(station);
        }

		public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Station station)
        {
            if (ModelState.IsValid)
            {
                db.CreateStation(station);

                return RedirectToAction("Index");
            }

            return View(station);
        }

		public ActionResult Edit(int id = 0)
		{
			Station station = db.GetStation(id);
			if (station == null)
			{
				return HttpNotFound();
			}
			return View(station);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Edit(Station station)
		{
			if (ModelState.IsValid)
			{
				db.EditStations(station);
				return RedirectToAction("Index");
			}
			return View(station);
		}

		public ActionResult Delete(int id = 0)
        {
            return View(db.GetStation(id));
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, Station station)
        {
            try
            {
                db.DeleteStation(id);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

		protected override void Dispose(bool disposing)
		{
			db.Dispose();
			base.Dispose(disposing);
		}
	}
}