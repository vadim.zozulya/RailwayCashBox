﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using RailwayCashBox.DAL.Entities;
using RailwayCashBox.DAL.Repositories;
using RailwayCashBox.Models;

namespace RailwayCashBox.Controllers
{
	[Authorize(Roles = "user")]
	public class BookingController : Controller
	{
        private Repository db = new Repository();

		public ActionResult Orders()
		{
			return View(db.GetOrders(User.Identity.GetUserId()));
		}

		public ActionResult StartBooking(int id)
		{
			var reset = Request.UrlReferrer == null || Request.UrlReferrer.Segments.Skip(1).Take(1).FirstOrDefault() != "Booking/";
			if (reset)
				Session["BookingProgress"] = null;

			var bookingProgress = Session["BookingProgress"] as BookingProgress;
			if (bookingProgress == null || bookingProgress.TrainScheduleRecord.Id != id)
			{
				var record = db.GetTrainScheduleRecord(id);
				bookingProgress = new BookingProgress
				{
					TrainScheduleRecord = record
				};

				foreach (var t in record.SeatBookings.Where(s => s.Booked))
				{
					bookingProgress.PreviouslyBookedSeats.Add(string.Format(BookingProgress.SeatNumberFormat, t.TrainCar.Number, t.SeatNumber));
				}
			}

			Session["BookingProgress"] = bookingProgress;

			return View(bookingProgress);
		}

		public ActionResult PrebookSeat(int schedulerecordid, int carnumber, int seatid)
		{
			var bookingProgress = (BookingProgress)Session["BookingProgress"];

			bookingProgress.ChoosenSeats.Add(string.Format(BookingProgress.SeatNumberFormat, carnumber, seatid));
			bookingProgress.ChoosenSeats = bookingProgress.ChoosenSeats.OrderBy(s => s).ToList();
			return RedirectToAction("StartBooking", new { id = schedulerecordid });
		}

		public ActionResult FinishBooking()
		{
			var bookingProgress = (BookingProgress)Session["BookingProgress"];
			var order = new OrderModel
			{
				SeatsToBook = bookingProgress.ChoosenSeats,
				TrainCode = bookingProgress.TrainScheduleRecord.TrainRoute.Code,
				DepartureStationName = bookingProgress.TrainScheduleRecord.TrainRoute.StartStation.Station.Name,
				ArrivalStationName = bookingProgress.TrainScheduleRecord.TrainRoute.EndStation.Station.Name,
				DepartureDate = bookingProgress.TrainScheduleRecord.DepartureDate,
				ArrivalDate = bookingProgress.TrainScheduleRecord.ArrivalDate
			};

			var seatBookings = new List<SeatBooking>();
			foreach (var choosenSeat in bookingProgress.ChoosenSeats)
			{
				var carNumber = int.Parse(choosenSeat.Split('-').First());
				var seatNumber = int.Parse(choosenSeat.Split('-').Last());

				var booking = bookingProgress.TrainScheduleRecord.SeatBookings.First(s => s.TrainCar.Number == carNumber && s.SeatNumber == seatNumber);
				seatBookings.Add(booking);
			}

			order.TotalPrice = seatBookings.Sum(s => s.TrainCar.SeatPrice);
			return View(order);
		}

		[HttpPost]
		public ActionResult FinishBooking(OrderModel orderModel)
		{
			var bookingProgress = (BookingProgress)Session["BookingProgress"];
			var user = User.Identity.GetUserId();

			var order = new Order();

			var seatBookings = new List<SeatBooking>();
			foreach (var choosenSeat in bookingProgress.ChoosenSeats)
			{
				var carNumber = int.Parse(choosenSeat.Split('-').First());
				var seatNumber = int.Parse(choosenSeat.Split('-').Last());

				var booking = bookingProgress.TrainScheduleRecord.SeatBookings.First(s => s.TrainCar.Number == carNumber && s.SeatNumber == seatNumber);
				seatBookings.Add(booking);
			}

			order.TrainScheduleRecord = bookingProgress.TrainScheduleRecord;
			order.SeatBookings = seatBookings;
			order.TotalPrice = orderModel.TotalPrice;
			order.UserId = user;

			foreach (var seatBooking in order.SeatBookings)
			{
				seatBooking.Booked = true;
			}

			db.SaveOrder(order);

			return RedirectToAction("Orders");
		}

		public ActionResult DeleteOrder(int id)
		{
			db.DeleteOrder(id);
			return RedirectToAction("Orders");
		}

		protected override void Dispose(bool disposing)
		{
			db.Dispose();
			base.Dispose(disposing);
		}

		public ActionResult TrainRouteDetails(int id)
		{
			return RedirectToAction("Details", "TrainRoute", new {id});
		}
	}
}