﻿using System;
using System.Web.Mvc;
using RailwayCashBox.DAL.Repositories;
using RailwayCashBox.Models;

namespace RailwayCashBox.Controllers
{
	public class HomeController : Controller
	{
		private Repository db = new Repository();

		public ActionResult Index()
		{
	        ViewBag.Stations = db.GetAllStations();
			return View(new SearchScheduleModel() {StartDate = DateTime.Today, EndDate = DateTime.Today.AddDays(3) });
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Index(SearchScheduleModel searchScheduleModel)
		{
	        ViewBag.Stations = db.GetAllStations();
			var t = searchScheduleModel.EndDate - searchScheduleModel.StartDate;
			if (ModelState.IsValid && searchScheduleModel.StartDate < DateTime.Today)
			{
				ModelState.AddModelError("", "Начало перида должно быть текущей датой или позже");
				return View(searchScheduleModel);
			}
			if (ModelState.IsValid && searchScheduleModel.EndDate.HasValue && searchScheduleModel.StartDate > searchScheduleModel.EndDate.Value)
			{
				ModelState.AddModelError("", "Начало перида должно быть меньше конца периода");
				return View(searchScheduleModel);
			}

			if (ModelState.IsValid)
			{
				var records = db.GetScheduleForStationsAndDate(searchScheduleModel.StartStationId, searchScheduleModel.EndStationId, searchScheduleModel.StartDate, searchScheduleModel.EndDate);
				searchScheduleModel.TrainScheduleRecords = records;
			}
			return View(searchScheduleModel);
		}

		public ActionResult Book(int id)
		{
			return RedirectToAction("StartBooking", "Booking", new {id});
		}

		public ActionResult TrainRouteDetails(int id)
		{
			return RedirectToAction("Details", "TrainRoute", new {id});
		}
	}
}