﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using RailwayCashBox.DAL.Entities;
using RailwayCashBox.DAL.Repositories;
using RailwayCashBox.Models;

namespace RailwayCashBox.Controllers
{
	[Authorize(Roles = "manager, admin")]
	public class TrainRouteController : Controller
    {
        private Repository db = new Repository();

        public ActionResult Index()
        {
            return View(db.GetRoutes());
        }

		[AllowAnonymous]
		public ActionResult Details(int id)
        {
            var trainroute = db.GetTrainRoute(id);
            if (trainroute == null)
            {
                return HttpNotFound();
            }
            return View(trainroute);
        }

	    public ActionResult CreateDetails()
	    {
			return View();
		}

		[HttpPost]
        [ValidateAntiForgeryToken]
		public ActionResult CreateDetails(TrainRouteCreateDetails details)
		{
			if (ModelState.IsValid)
			{
                return RedirectToAction("Create", details);
			}
			return View();
		}

		public ActionResult Create(TrainRouteCreateDetails details)
        {
            ViewBag.Stations = new SelectList(db.GetAllStations(), "Id", "Name");

			var trainRoute = new TrainRoute() {StartDate = DateTime.Today};
			trainRoute.StartStation.DepartureTime = DateTime.Today;
			trainRoute.EndStation.ArrivalTime = DateTime.Today;
			for (int i = 0; i < details.NumberOfIntermediateStations; i++)
				trainRoute.IntermediateStations.Add(new StationStop());

			for (int i = 0; i < details.NumberOfCars; i++)
				trainRoute.TrainCars.Add(new TrainCar());

			return View(trainRoute);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(TrainRoute trainroute)
        {
            ViewBag.Stations = new SelectList(db.GetAllStations(), "Id", "Name");

	        var isvalid = true;
	        if (trainroute.StartStation.DepartureTime == null)
	        {
		        ModelState.AddModelError("", "Начальная станция не имеет времени отправления");
		        isvalid = false;
	        }
	        if (trainroute.EndStation.ArrivalTime == null)
	        {
		        ModelState.AddModelError("", "Конечная станция не имеет времени прибытия");
				isvalid = false;
			}
			if (trainroute.IntermediateStations.Any(s => !s.ArrivalTime.HasValue || !s.DepartureTime.HasValue))
	        {
		        ModelState.AddModelError("", "В промежуточных станциях неправильно проставлены даты");
				isvalid = false;
			}

			if (!isvalid)
				return View(trainroute);

			if (ModelState.IsValid)
			{
				var stations = db.GetAllStations();

				trainroute.StartStation.Station = stations.First(s => s.Id == trainroute.StartStation.StationIdForEditing);
				trainroute.EndStation.Station = stations.First(s => s.Id == trainroute.EndStation.StationIdForEditing);
				foreach (var intermediateStation in trainroute.IntermediateStations)
				{
					intermediateStation.Station = stations.First(s => s.Id == intermediateStation.StationIdForEditing);
				}

				db.CreateTrainRoute(trainroute);
                return RedirectToAction("Index");
            }


			return View(trainroute);
        }
        
        public ActionResult Edit(int id = 0)
        {
	        ViewBag.Stations = db.GetAllStations();
			TrainRoute trainroute = db.GetTrainRoute(id);
            if (trainroute == null)
            {
                return HttpNotFound();
            }
            return View(trainroute);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(TrainRoute trainroute)
        {
            if (ModelState.IsValid)
            {
                db.EditTrainRoute(trainroute);

                return RedirectToAction("Index");
            }
            ViewBag.Stations = new SelectList(db.GetAllStations(), "Id", "Name");

			return View(trainroute);
        }

        // GET: /TrainRoute/Delete/5
        public ActionResult Delete(int id)
        {
            return View(db.GetTrainRoute(id));
        }

        // POST: /TrainRoute/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, TrainRoute trainRoute)
        {
            db.DeleteTrainRoute(id);
            return RedirectToAction("Index");
        }

	    public ActionResult ShowSchedule(int id)
	    {
		    var model = new TrainSchedule
		    {
			    TrainRouteId = id,
			    Records = db.GetSchedule(id)
                    .Where(r => r.DepartureDate < DateTime.Now.AddMonths(3))
                    .OrderBy(r => r.DepartureDate)
                    .ToList()
		    };

		    return View(model);
	    }

	    public ActionResult ClearScheduleForRoute(int id)
	    {
		    db.ClearScheduleForRoute(id);
		    return RedirectToAction("ShowSchedule", new {id});
	    }

	    public ActionResult DeleteTrainScheduleRecord(int id)
	    {
			var record = db.GetTrainScheduleRecord(id);
		    var routeId = record.TrainRoute.Id;
		    db.DeleteTrainScheduleRecord(id);

		    return RedirectToAction("ShowSchedule", new {id = routeId});
	    }

	    public ActionResult GenerateScheduleDetailsForRoute(int id)
	    {
		    return View(new GenerateScheduleDetails {RouteId = id,FromDate = DateTime.Today, ToDate = DateTime.Today});
	    }

        [HttpPost]
		public ActionResult GenerateScheduleDetailsForRoute(GenerateScheduleDetails generateScheduleDetails)
        {
	        if (ModelState.IsValid)
	        {
		        var route = db.GetTrainRoute(generateScheduleDetails.RouteId);
		        var records = TrainScheduleHelper.CreateTrainScheduleRecords(route, generateScheduleDetails.FromDate,
			        generateScheduleDetails.ToDate);
		        db.SaveSchedule(records);
		        return RedirectToAction("ShowSchedule", new {id = generateScheduleDetails.RouteId});
	        }
	        return View(generateScheduleDetails);
        }

		protected override void Dispose(bool disposing)
		{
			db.Dispose();
			base.Dispose(disposing);
		}
	}

	public static class BookingHelper
	{
		public static BookingState GetBookingState(string currentSeat, List<string> previouslyBooked, List<string> choosen)
		{
			if(previouslyBooked.Contains(currentSeat))
				return BookingState.Booked;
			if (choosen.Contains(currentSeat))
				return BookingState.Chosen;

			return BookingState.Free;
		}
	}

	public enum BookingState
	{
		Free,
		Booked,
		Chosen
	}
}