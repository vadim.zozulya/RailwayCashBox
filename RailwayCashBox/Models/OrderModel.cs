﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RailwayCashBox.Models
{
	public class OrderModel
	{
		[Display(Name = "Поезд")]
		public string TrainCode { get; set; }
		[Display(Name = "Станция отправления")]
		public string DepartureStationName { get; set; }
		[Display(Name = "Станиция прибытия")]
		public string ArrivalStationName { get; set; }
		[Display(Name = "Места")]
		public List<string> SeatsToBook { get; set; }
		[Display(Name = "Общая стоимость")]
		public decimal TotalPrice { get; set; }
		[Display(Name = "Время отправления")]
		[DataType(DataType.DateTime)]
		public DateTime DepartureDate { get; set; }
		[Display(Name = "Время прибытия")]
		[DataType(DataType.DateTime)]
		public DateTime ArrivalDate { get; set; }
	}
}