﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace RailwayCashBox.Models
{
	public class GenerateScheduleDetails
	{
		public int RouteId { get; set; }
		[Display(Name = "Начало периода")]
		[DataType(DataType.Date)]
		[Required]
		public DateTime FromDate { get; set; }
		[Display(Name = "Конец периода")]
		[DataType(DataType.Date)]
		[Required]
		public DateTime ToDate { get; set; }
	}
}