﻿using System.Collections.Generic;
using RailwayCashBox.DAL.Entities;

namespace RailwayCashBox.Models
{
	public class BookingProgress
	{
		public const string SeatNumberFormat = "{0}-{1}"; //номер вагона - ID места

		public BookingProgress()
		{
			ChoosenSeats = new List<string>();
			PreviouslyBookedSeats = new List<string>();
		}

		public TrainScheduleRecord TrainScheduleRecord { get; set; }

		public List<string> PreviouslyBookedSeats { get; set; }

		public List<string> ChoosenSeats { get; set; }
	}
}