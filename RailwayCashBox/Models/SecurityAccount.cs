﻿using System.ComponentModel.DataAnnotations;

namespace RailwayCashBox.Models
{
	public class SecurityAccount
	{
		public string UserId { get; set; }
		[Display(Name = "Имя пользователя")]
		public string UserName { get; set; }
		[Display(Name = "Пароль")]
		[DataType(DataType.Password)]
		public string Password { get; set; }
	}
}