﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using RailwayCashBox.DAL.Entities;

namespace RailwayCashBox.Models
{
	public class TrainRouteCreateDetails
	{
		[Required]
		[Display(Name = "Количество промежуточных станций")]
		public int NumberOfIntermediateStations { get; set; }
		[Required]
		[Display(Name = "Количество вагонов")]
		public int NumberOfCars { get; set; }
	}

	public class TrainSchedule
	{
		public int TrainRouteId { get; set; }
		public IList<TrainScheduleRecord> Records { get; set; }
	}
}