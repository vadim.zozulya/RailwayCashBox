﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using RailwayCashBox.DAL.Entities;

namespace RailwayCashBox.Models
{
	public class SearchScheduleModel
	{
		[Required]
		[DataType(DataType.Date)]
		[Display(Name = "С")]
		public DateTime StartDate { get; set; }
		[DataType(DataType.Date)]
		[Display(Name = "По")]
		public DateTime? EndDate { get; set; }
		[Required]
		[Display(Name = "Станция отправления")]
		public int StartStationId { get; set; }
		[Required]
		[Display(Name = "Станция прибытия")]
		public int EndStationId { get; set; }
		[Display(Name = "Найденный маршруты")]
		public List<TrainScheduleRecord> TrainScheduleRecords { get; set; }
	}
}