﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RailwayCashBox
{
	public static class Helper
	{
		public static List<SelectListItem> GetEnumValues(Type enumType, object selectedValue, Func<Enum, string> toDescr)
		{
			var enumValues = Enum.GetValues(enumType);
			var selectedEnumValue = selectedValue == null ? "" : selectedValue.ToString();

			var selectListItems = new List<SelectListItem>();
			foreach (var enumValue in enumValues)
			{
				if((int)enumValue == 0)
					continue;

				var enumOptionName = Enum.GetName(enumType, enumValue);
				selectListItems.Add(new SelectListItem
				{
					Text = toDescr == null ? enumOptionName : toDescr(enumValue as Enum),
					Value = enumOptionName,
					Selected = enumOptionName == selectedEnumValue
				});
			}
			return selectListItems;
		}
	}
}